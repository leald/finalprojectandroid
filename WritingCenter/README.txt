Project Name: CarlWrite
=======================

Clients 
=======
Erik Warren and Kathy Evertz

Team Members
============
	Android Development: Danny Leal and Kyung Ho Song
	iOS Development: Quang Tran and Anmol Raina

Description
===========
CarlWrite is an app that is designed to help students in Carleton College, MN 
easily make and cancel appointment with the Carleton Writing Center.
You can log in with user id (songk, leald or ondichj) and the password for all
ids is "password".

Set up for Caldroid(The custom date picker)
===========================================
(From Caldroid README.md file)

**For Eclipse/ADT user**: please see tag [eclipse_project](https://github.com/roomorama/Caldroid/releases/tag/eclipse_project), 
download the source codes, check out the CaldroidSample to see how the library works. 
However you are strongly recommended to use Maven or gradle, because this tag is no longer supported.

To use in your project, reference the child library project as a library. 
If you see JAR mismatched error, replace your android-support-v4.jar to the jar inside Caldroid. 
Make sure you compile the project against Android 4.2 and above to allow nested fragment. 
See more at http://developer.android.com/about/versions/android-4.2.html#NestedFragments

**For Android Studio user**: add `compile 'com.roomorama:caldroid:2.0.0'` to your gradle build file.

**For Maven user**:
```
<dependency>
    <groupId>com.roomorama</groupId>
    <artifactId>caldroid</artifactId>
    <version>2.0.0</version>
</dependency>
```

From here to deployment
=======================
1. Server Issues
Currently, we do not have access to the server so we are making a fake "server class" that we hard coded everything into. In the deployment version all classes using our fake server class have to be altered. These classes include,
	AppointmentInfoActivity
	ChooseDateActivity
	HomeActivity
	makeAppointmentActivty
	LoginActivty
	Server
	User
Look for the javadoc/comments in these specific classes on our idea of how
and where the real server access might work. However, keep in mind that we have
no idea what kind of data we would expect in these servers, or what kind of authentification we can get from them. For example, we cannot say that filtering by tutor names that we implemented in ChooseDateActivity is even possible without changing the server.

2. Push notification
Currently push notification is not implemented, but it was in the clients' optional features. To implement you need access to the server and the device (you need to
get registration id from google which has to be stored in your device and on the server)
which we don't have (server access).

3. Tie in with Calendar app
Currently not implemented, but it was in the clients' optional features.

4. Sending e-mails to the tutors with info from appointments made
For the reasons stated in #1, this is currently not implemented. Also see into the javadoc and comments of the specific classes on where it needs to occur.

5. Disclosure arrow
The list in the HomeActivity class should have disclosure arrow next to 
the appointments made.

6. Highlighting dates
Currently we are highlighting all dates as available, but once server access is
implemented, we would like to allow users to only select dates that Erik
schedules.
If a user selects a specific tutor to work with, we would like the calendar
to highlight only the dates that tutor works and populate the timeslots
accordingly.
