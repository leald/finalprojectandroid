package edu.dannylealcarleton.writingcenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by dannyleal on 5/12/15.
 * Activity class for looking up info of already made appointment.
 * We are only showing the date, the chosen time slot and the tutor name.
 */
public class AppointmentInfoActivity extends BaseMenu {
    private static String name;
    private static String date;
    private static String chosenTime;
    private static String chosenTutor;
    private static Appointment currentAppointment;
    private static int appointmentPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_info);
        //Getting the specified appointment from HomeActivity
        currentAppointment = (Appointment)getIntent().getSerializableExtra("appointment");

        /**
        currentAppointment = serverQuery(); will be used when server access is implemented.
         */
        serverQuery();

        //this position is only used for deleting the correct appointment. After we go back
        //to HomeActivity
        appointmentPosition = (int)getIntent().getSerializableExtra("position");

        TextView dateText = (TextView)findViewById(R.id.dateOfAppointment);
        TextView timeText = (TextView)findViewById(R.id.chosenTime);
        TextView tutorText = (TextView)findViewById(R.id.chosenTutor);

        //These four variables may get their info directly from server when the app gets access to it.
        this.name = currentAppointment.getAppointmentName();
        this.date = currentAppointment.getAppointmentDate();
        this.chosenTime = currentAppointment.getAppointmentTime();
        this.chosenTutor = currentAppointment.getSpecifiedTutor();

        dateText.setText("Appointment date: "+ date);
        timeText.setText("Time: " + chosenTime);
        tutorText.setText("Tutor: " + chosenTutor);
    }

    /**
     * This method will ask server to return the appointment we are looking for.
     * May need to construct a appointment instance with received info.
     * @return an appointment instances
     */
    public Appointment serverQuery() {
        return null;
    }

    /**
     *This method will request the server to cancel the appointment
     * and possibly send the cancellation confirmation email to the user and
     * to the tutor.
     */
    public void requestCancel() {

    }

    public void cancelAppointment(View v) {
        finishWithCancelAppointment();
    }

    private void finishWithCancelAppointment() {
        requestCancel();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("appointment", currentAppointment);
        returnIntent.putExtra("position", appointmentPosition);

        this.setResult(RESULT_OK, returnIntent);
        this.finish();
    }
}