package edu.dannylealcarleton.writingcenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dannyleal on 5/11/15.
 * Activity that handles tutor, date, and time picking.
 */
public class ChooseDateActivity extends BaseMenu {
    //instance variable for custom date picker (called Caldroid)
    private CaldroidFragment caldroidFragment;
    private SimpleDateFormat standardDateFormat = new SimpleDateFormat("E, d MMM");
    private Date previousDate;

    private Spinner tutors;
    private Spinner availableTimeOfDay;

    private ArrayAdapter<String> adapter;

    //This is used to simulate server. Will replaced with what is returned
    //from serverQuery() in the deployment version of the app.
    private Server dataSource = new Server();
    private String formattedDate;

    /**
     * This method sets up caldroid. Currently sets min date and max date,
     * In the deployment version, use caldroidFragment.setDisableDatesFromString
     * method to only enable dates that Erik(Client) has scheduled (associated with tutors)
     * SetDisableDatesFromString method takes in string of dates. The input date
     * format uses yyyy-MM-dd as default, but you can also pass in other date formats
     * as the second argument to the method.
     */
    private void setResourceForDates() {
        serverQuery();
        Calendar cal = Calendar.getInstance();
        previousDate = cal.getTime();

        //set min date (today)
        Date minDate = cal.getTime();
        //set max date (1 month from now)
        cal.add(Calendar.MONTH, 2);
        Date maxDate = cal.getTime();

        if (caldroidFragment != null) {
            caldroidFragment.setMinDate(minDate);
            //TODO: Replace with your logic to set disabled dates. ServerQuery should be used here to get the dates.
            caldroidFragment.setMaxDate(maxDate);
        }
    }

    /**
     * returns requested info to the activity from the server.
     * May need to be overloaded (to incorporate getting tutor, dates, and time slots
     * currently returns nothing.
     * TODO: fill it up with your logic
     */
    private void serverQuery() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date);

        //set up caldroid
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefault);
        caldroidFragment.setArguments(args);
        setResourceForDates();

        //attach caldroid fragment to the activity
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction t = fragmentManager.beginTransaction();
        t.replace(R.id.datePicker, caldroidFragment);
        t.commit();

        //set up listener for caldroid
        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                caldroidFragment.clearBackgroundResourceForDate(previousDate);
                previousDate = date;
                formattedDate = standardDateFormat.format(date.getTime());
                //changing the highlight color to other colors will crash the app. We have NO IDEA why that is the case.
                caldroidFragment.setBackgroundResourceForDate(0x000000, date);
                caldroidFragment.refreshView();
                //Updates available time slots based on the date currently highlighted
                fillTimeSlots(dataSource.getAvailableTimeSlots());
            }
        };
        caldroidFragment.setCaldroidListener(listener);

        //set up a spinner for the tutors
        tutors = (Spinner)findViewById(R.id.tutorSpinner);
        fillTutors(dataSource.getListOfTutors());

        //set up spinner for the time slots
        availableTimeOfDay = (Spinner)findViewById(R.id.timeSpinner);
        availableTimeOfDay.setOnItemSelectedListener(new itemSelectedListener());
        fillTimeSlots(dataSource.getAvailableTimeSlots());

        //formatted date is the output from standardDateFormat (which takes in a date object and formats it to look nice)
        formattedDate = standardDateFormat.format(cal.getTime());
    }


    /**
     * fillTimeSlots method creates fake time slots that users will be able to choose from.
     * @param serverData: the server date list requested by the serverQuery method
     * fills a spinner
     */
    private void fillTimeSlots(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> timeSlots = new ArrayList<>();
        timeSlots.add("Choose a time");
        for(int i = 0; i < serverData.size(); i++) {
            timeSlots.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, timeSlots);
        availableTimeOfDay.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    /**
     * another class to fill a spinner using fake tutors.
     * @param serverData: the server tutor list requested by the serverQuery method
     * currently uses hardcoded tutors.
     */
    private void fillTutors(ArrayList<String> serverData) {
        ArrayList<String> tutorSlots = new ArrayList<>();
        tutorSlots.add("Any Tutor");
        for(int i=0; i < serverData.size(); i++) {
            tutorSlots.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, tutorSlots);
        tutors.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    /**
     * This class handles spinner selection(for the time slot spinner). Once you choose time, it will navigate
     * automatically to makeAppointmentActivity
     */
    private class itemSelectedListener implements Spinner.OnItemSelectedListener {
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {

        }

        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            if (!parentView.getSelectedItem().toString().equals("Choose a time")) {
                Intent intent = new Intent(ChooseDateActivity.this, MakeAppointmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                intent.putExtra("date", formattedDate);
                intent.putExtra("tutor", tutors.getSelectedItem().toString());
                intent.putExtra("time", availableTimeOfDay.getSelectedItem().toString());
                startActivity(intent);
                finish();
            }
        }
    }
}

