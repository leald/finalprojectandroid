package edu.dannylealcarleton.writingcenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by K Song on 5/28/2015.
 * A class that handles making appointment/filling up info. Currently DOES NOT handle info coming in from
 * check boxes.
 */
public class MakeAppointmentActivity extends BaseMenu {
    private EditText appointmentName;
    private EditText course;
    private EditText instructor;
    private EditText topic;

    private String formattedDate;
    private String specifiedTutor;
    private String chosenTime;

    private ArrayAdapter<String> adapter;

    private Server dataSource = new Server();

    //Spinners
    private Spinner projectType;
    private Spinner dueDate;
    private Spinner writingProcess;
    private Spinner numberOfPages;
    private Spinner classYear;
    private Spinner major;
    private Spinner copy;
    private Spinner firstVisit;
    private Spinner learnAboutUs;

    //checkboxes
    private CheckBox clarity, thesis, organization, citation, brainstorm, evidence, proofread;

    private void sendToServer() {
        //TODO: request the server to make the appointment with populated info. Send email to tutors with populated info
    }

    @Override
    protected void onCreate(Bundle savedinstanceState) {
        super.onCreate(savedinstanceState);
        setContentView(R.layout.activity_make_appointment);

        Intent parentIntent = getIntent();
        formattedDate = parentIntent.getStringExtra("date");
        specifiedTutor = parentIntent.getStringExtra("tutor");
        chosenTime = parentIntent.getStringExtra("time");


        this.appointmentName = (EditText)findViewById(R.id.nameAppointment);
        this.course = (EditText)findViewById(R.id.course);
        this.instructor = (EditText)findViewById(R.id.instructor);
        this.topic = (EditText)findViewById(R.id.topic);

        //Start to create spinners
        this.projectType = (Spinner)findViewById(R.id.project_type);
        this.dueDate = (Spinner)findViewById(R.id.due);
        this.writingProcess = (Spinner)findViewById(R.id.process);
        this.numberOfPages = (Spinner)findViewById(R.id.how_many_pages);
        this.classYear = (Spinner)findViewById(R.id.class_year);
        this.major = (Spinner)findViewById(R.id.major);
        this.copy = (Spinner)findViewById(R.id.copy);
        this.firstVisit = (Spinner)findViewById(R.id.first_time);
        this.learnAboutUs = (Spinner)findViewById(R.id.how_did_you_hear_about);

        //start to create check boxes
        this.clarity = (CheckBox)findViewById(R.id.clarity);
        this.thesis = (CheckBox)findViewById(R.id.thesis);
        this.organization = (CheckBox)findViewById(R.id.organization);
        this.citation = (CheckBox)findViewById(R.id.citation);
        this.brainstorm = (CheckBox)findViewById(R.id.brainstorm);
        this.evidence = (CheckBox)findViewById(R.id.evidence);
        this.proofread = (CheckBox)findViewById(R.id.proofread);

        //fill spinners
        fillProjectType(dataSource.getProjectTypes());
        fillDueDate(dataSource.getDueDates());
        fillWritingProcess(dataSource.getWritingProcesses());
        fillNumberOfPages(dataSource.getNumberOfPages());
        fillClassYear(dataSource.getClassYears());
        fillMajor(dataSource.getMajors());
        fillCopy(dataSource.getYesOrNo());
        fillFirstVisit(dataSource.getYesOrNo());
        fillLearnAboutUs(dataSource.getLearnAboutUs());
    }

    /**
     * each one of these fill... methods fills up a specific spinner
     * @param serverData the arrays containing data meant to be put into the spinner
     */
    private void fillProjectType(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.projectType.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillDueDate(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.dueDate.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillWritingProcess(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.writingProcess.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillNumberOfPages(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.numberOfPages.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillClassYear(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.classYear.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillMajor(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.major.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillCopy(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.copy.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillFirstVisit(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.firstVisit.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void fillLearnAboutUs(ArrayList<String> serverData) {
        //hardcoded fake time slots until we get server access
        ArrayList<String> array = new ArrayList<>();
        array.add("---");
        for(int i = 0; i < serverData.size(); i++) {
            array.add(serverData.get(i));
        }
        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, array);
        this.learnAboutUs.setAdapter(adapter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    /**
     * This is called on a press of the submit button. This will call sendToServer() to send data to
     * the server and email tutors(needs to be implemented) and will close the activity. Almost none of this will exist as it currently
     * is.
     */
    private void finishWithAppointmentName() {
        sendToServer();
        Intent returnIntent = new Intent();
        if(TextUtils.isEmpty(appointmentName.getText())){
            appointmentName.setText(formattedDate);
        }
        /**
         * TODO: Add the created appointment to the user's appointment list instead of passing it in as an extra
         *
         * Currently we cannot add appointments to user's appointment list because we do not have mutable data base
         * that we can read from or write into.
         */
        ArrayList<Boolean> checkboxAnswers = new ArrayList();
        checkboxAnswers.add(clarity.isChecked());
        checkboxAnswers.add(thesis.isChecked());
        checkboxAnswers.add(organization.isChecked());
        checkboxAnswers.add(citation.isChecked());
        checkboxAnswers.add(brainstorm.isChecked());
        checkboxAnswers.add(evidence.isChecked());
        checkboxAnswers.add(proofread.isChecked());
        returnIntent.putExtra("appointment", new Appointment(appointmentName.getText().toString(),
                formattedDate, chosenTime, specifiedTutor, this.course.getText().toString(), this.instructor.getText().toString(), this.topic.getText().toString(),
                this.projectType.getSelectedItem().toString(), this.dueDate.getSelectedItem().toString(), this.writingProcess.getSelectedItem().toString(), this.numberOfPages.getSelectedItem().toString(),
                this.classYear.getSelectedItem().toString(), this.major.getSelectedItem().toString(), this.copy.getSelectedItem().toString(), this.firstVisit.getSelectedItem().toString(),
                this.learnAboutUs.getSelectedItem().toString(), checkboxAnswers));
        this.setResult(RESULT_OK, returnIntent);
        this.finish();
    }

    public void returnAppointment(View view) {
        finishWithAppointmentName();
    }

}
