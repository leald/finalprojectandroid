package edu.dannylealcarleton.writingcenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by K Song on 5/26/2015.
 * Fake server with hardcoded data.
 */
public class Server implements Serializable {
    private ArrayList<String> ListOfTutors;
    private ArrayList<String> availableTimeSlots;
    private ArrayList<String> projectTypes;
    private ArrayList<String> dueDates;
    private ArrayList<String> writingProcesses;
    private ArrayList<String> numberOfPages;
    private ArrayList<String> classYears;
    private ArrayList<String> majors;
    private ArrayList<String> yesOrNo;
    private ArrayList<String> learnAboutUs;

    private Hashtable<String, User> studentUsers;
    private String DEFAULT_PASSWORD = "password";

    public Server() {
        this.studentUsers = new Hashtable<>();


        //hardcode list of tutors
        ListOfTutors = new ArrayList<>();
        ListOfTutors.add("Sam");
        ListOfTutors.add("Quang");
        ListOfTutors.add("Anmol");

        //hardcode available time slots
        availableTimeSlots = new ArrayList<>();
        availableTimeSlots.add("1:30 PM");
        availableTimeSlots.add("3:30 PM");
        availableTimeSlots.add("8:30 PM");
        availableTimeSlots.add("9:00 PM");

        //hardcode project types
        projectTypes = new ArrayList<>();
        projectTypes.add("Research Paper");

        //hardcode due dates
        dueDates = new ArrayList<>();
        dueDates.add("This Week");
        dueDates.add("One Week");
        dueDates.add("Two Weeks");

        //hardcode how far in the writing process
        writingProcesses = new ArrayList<>();
        writingProcesses.add("Just Started");
        writingProcesses.add("Nearly Finished");

        //hardcocde number of pages
        numberOfPages = new ArrayList<>();
        numberOfPages.add("1-3");
        numberOfPages.add("4-5");
        numberOfPages.add("6-10");
        numberOfPages.add("Greater than 10");


        //hardcode class years
        classYears = new ArrayList<>();
        classYears.add("2015");
        classYears.add("2016");
        classYears.add("2017");
        classYears.add("2018");

        //hardcode majors
        majors = new ArrayList<>();
        majors.add("Computer Science");
        majors.add("Why would you not be a CS major?");

        //hardcode yes and no spinner
        yesOrNo = new ArrayList<>();
        yesOrNo.add("Yes");
        yesOrNo.add("No");

        //hardcode how the user learned about us?
        learnAboutUs = new ArrayList<>();
        learnAboutUs.add("From a friend");
        learnAboutUs.add("Who doesn't know about the writing center?");


        //Create users hardcoded
        User danny = new User("leald", DEFAULT_PASSWORD);
        User kyung = new User("songk", DEFAULT_PASSWORD);
        User jeff = new User("ondichj", DEFAULT_PASSWORD);

        //Populate our dictionary of users (keys = username, value = user object)
        studentUsers.put(danny.getUsername(), danny);
        studentUsers.put(kyung.getUsername(), kyung);
        studentUsers.put(jeff.getUsername(), jeff);

    }
    public ArrayList<String> getListOfTutors() {
        return ListOfTutors;
    }

    public ArrayList<String> getAvailableTimeSlots() {return availableTimeSlots;}

    public ArrayList<String> getProjectTypes() {
        return projectTypes;
    }

    public ArrayList<String> getDueDates() {
        return dueDates;
    }

    public ArrayList<String> getWritingProcesses() {
        return writingProcesses;
    }

    public ArrayList<String> getNumberOfPages() {
        return numberOfPages;
    }

    public ArrayList<String> getClassYears() {
        return classYears;
    }

    public ArrayList<String> getMajors() {
        return majors;
    }

    public ArrayList<String> getYesOrNo() {
        return yesOrNo;
    }

    public ArrayList<String> getLearnAboutUs() {
        return learnAboutUs;
    }

    public Hashtable<String, User> getUserList() {
        return this.studentUsers;
    }
}
