package edu.dannylealcarleton.writingcenter;

import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by K Song on 5/27/2015.
 * This is the class for individual appointments. Every appointment will be saved
 * as an instance of this class. This class may need to add more features or
 * not even be necessary if the app gets server access.
 */
public class Appointment implements Serializable{
    private String appointmentName;
    private String appointmentDate;
    private String appointmentTime;
    private String specifiedTutor;
    private String projectType;
    private String dueDate;
    private String writingProcess;
    private String numberOfPages;
    private String classYear;
    private String major;
    private String copy;
    private String firstVisit;
    private String learnAboutUs;
    private ArrayList<Boolean> checkboxAnswers;

    public Appointment(String name, String date, String time, String tutor, String course, String instructor, String topic, String projectType, String dueDate,
                       String writingProcess, String numberOfPages, String classYear, String major, String copy, String firstVisit, String learnAboutUs, ArrayList<Boolean> checkboxAnswers) {
        this.appointmentName = name;
        this.appointmentDate = date;
        this.appointmentTime = time;
        this.specifiedTutor = tutor;
        this.projectType = projectType;
        this.dueDate = dueDate;
        this.writingProcess = writingProcess;
        this.numberOfPages = numberOfPages;
        this.classYear = classYear;
        this.major = major;
        this.copy = copy;
        this.firstVisit = firstVisit;
        this.learnAboutUs = learnAboutUs;
        this.checkboxAnswers = checkboxAnswers;

    }
    public String getAppointmentName() {
        return appointmentName;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public String getSpecifiedTutor() {
        return specifiedTutor;
    }

    public String getProjectType() { return projectType; }

    public String getDueDate() {return dueDate; }

    public String getWritingProcess() {return writingProcess; }

    public String getNumberOfPages() {return numberOfPages; }

    public String getClassYear() {return classYear; }

    public String getMajor() {return major; }

    public String getSendCopy() {return copy; }

    public String getFirstVisit() {return firstVisit; }

    public String getLearnAboutUs() {return learnAboutUs; }
}