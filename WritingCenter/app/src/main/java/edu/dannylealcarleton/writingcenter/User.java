package edu.dannylealcarleton.writingcenter;

import java.util.ArrayList;

/**
 * Created by dannyleal on 6/7/15.
 * A User class that will be in our fake server class. This
 * will not be necessary once we get the server access.
 */
public class User {
    private ArrayList<Appointment> writingAppointments;
    private String username;
    private String password;


    public User(String username, String password) {
        this.password = password;
        this.username = username;
        this.writingAppointments = new ArrayList<>();
    }

    public ArrayList<Appointment> getWritingAppointments() {
        return this.writingAppointments;
    }

    public void addNewAppointment(Appointment newApp) {
        this.writingAppointments.add(newApp);
    }

    public void removeAppointment(int position) {
        this.writingAppointments.remove(position);
    }

    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return this.username;
    }


}
