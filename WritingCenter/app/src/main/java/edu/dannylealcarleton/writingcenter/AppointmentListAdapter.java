package edu.dannylealcarleton.writingcenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dannyleal on 5/11/15.
 * a custom adapter for appointment list in the home activity
 */
public class AppointmentListAdapter extends ArrayAdapter<Appointment> {

    public AppointmentListAdapter(Context context, int resource, ArrayList<Appointment> listItems) {
        super(context, resource, listItems);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_cell, null);
        }

        if (v != null) {
            TextView textView = (TextView) v.findViewById(R.id.listCellTextView);
            textView.setText(this.getItem(position).getAppointmentName());
        }

        return v;
    }
}
