package edu.dannylealcarleton.writingcenter;

import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by dannyleal on 5/31/15.
 * This activity will navigate students to Writing Center web page.
 */
public class AboutActivity extends BaseMenu {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        WebView webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl("https://apps.carleton.edu/campus/asc/writingcenter/");
    }
}
