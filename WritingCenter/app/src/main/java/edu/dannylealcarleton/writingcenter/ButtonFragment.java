package edu.dannylealcarleton.writingcenter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by dannyleal on 5/11/15.
 *
 * A class for the button fragment contained in our HomeActivity
 */
public class ButtonFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.button_fragment, container, false);
        return view;
    }
}
