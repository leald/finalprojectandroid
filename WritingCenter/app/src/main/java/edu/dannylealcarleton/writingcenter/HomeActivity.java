package edu.dannylealcarleton.writingcenter;


import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;


/**
 * Created by dannyleal on 5/6/15.
 * Home page that contains a listActivity of all appointments currently scheduled by a user.
 * Gives the option of adding a new appointment (ChooseDateActivity -> makeAppointmentActivity) or deleting old ones/viewing their
 * details. (AppointmentInfoActivity)
 */
public class HomeActivity extends BaseMenu {
    public static final int APPT_REQUEST_CODE = 1;
    public static final int CANCEL_REQUEST_CODE = 2;
    public static ArrayList<Appointment> appointmentsMade = new ArrayList<>();
    public static AppointmentListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Remove back arrow for home activity. This forces a logout through the logout option
        //in the options menu
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        //Set up list adapter
        this.adapter = new AppointmentListAdapter(this, R.layout.list_cell, appointmentsMade);
        ListView listView = (ListView)this.findViewById(android.R.id.list);
        listView.setAdapter(this.adapter);

        //grab the appointments for this user.
        serverQuery();
    }

    /**
     * grab appointments to be displayed from the server.
      * @return An arrayList of appointment names.
     * This may be altered to take in appointment details and using those
     * info to make appointment class instances
     */
    public ArrayList<String> serverQuery(){
        //TODO: implement your own logic here (grab current user's list of appointments from the server
        return null;
    }

    /**
     * On a click of the "Add Appointment" button, creates a new intent (ChooseDateActivity) for result
     * @param view the button.
     */
    public void makeAppointment(View view) {
        Intent intent = new Intent(this, ChooseDateActivity.class);
        startActivityForResult(intent, HomeActivity.APPT_REQUEST_CODE);
    }

    /**
     * Once server access is implemented, all this method will be used for is to show toast since
     * we load new appointment on onCreate and everytime we make or delete an appointment
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case HomeActivity.APPT_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    /**
                     * TODO: ServerQuery
                     * If the server access is granted we would to server query here again to show the list of appointments
                     * We currently don't have the server access, so we are using getExtra method.
                     */
                    Appointment appt = (Appointment)data.getSerializableExtra("appointment");
                    Toast toast = Toast.makeText(this, "Appointment "  + "'" + appt.getAppointmentName() + "'" + " Added" , Toast.LENGTH_SHORT);
                    toast.show();
                    //username/user.addAppointment
                    this.appointmentsMade.add(appt);
                    //Update the view
                    this.adapter.notifyDataSetChanged();
                }
                break;
            case HomeActivity.CANCEL_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    /**
                     * TODO: ServerQuery
                     * If the server access is granted, we wouldn't need to delete anything from the list. We would just
                     * request the current list of appointments here again to show. Since we don't have the server access now,
                     * we are deleting appointments locally.
                     */
                    Appointment appt = (Appointment)data.getSerializableExtra("appointment");
                    int appointmentPosition = (int)data.getSerializableExtra("position");
                    //User.removeAppointment
                    this.appointmentsMade.remove(appointmentPosition);
                    Toast toast = Toast.makeText(this, "Appointment " + "'" + appt.getAppointmentName() + "'" + " Cancelled", Toast.LENGTH_SHORT);
                    toast.show();
                    this.adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    /**
     * This class handles all list activities including clicks.
     */
    public static class AppointmentsFragment extends ListFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.list_fragment, container, false);
            return view;
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            Intent intent = new Intent(getActivity(), AppointmentInfoActivity.class);
            Appointment selected = (Appointment)l.getItemAtPosition(position);
            /**
             * We wouldn't need to give position here when we get the server access.
             */
            intent.putExtra("appointment", selected);
            intent.putExtra("position", position);
            getActivity().startActivityForResult(intent, HomeActivity.CANCEL_REQUEST_CODE);
        }
    }

}
